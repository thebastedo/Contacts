var faker = require('faker');
var path = require('path');
var fs = require('fs');

const HOW_MANY = 100;

const FILE = path.join(__dirname, 'demo-contacts.js');

var contacts = [];

for (var i=0; i < HOW_MANY; i++) {
  contacts.push({
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    company: faker.company.companyName(),
    phone: faker.phone.phoneNumber(),
    mobile: faker.phone.phoneNumber(),
    email: faker.internet.email(),
    birthday: faker.date.past(),
    address: faker.address.streetAddress(),
    city: faker.address.city(),
    sate: faker.address.stateAbbr(),
    zip: faker.address.zipCode()
  });
}

fs.writeFile(FILE, JSON.stringify(contacts));
console.log('done!');
