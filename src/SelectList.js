import React from 'react'
import classNames from 'classnames'

var SelectList = React.createClass({
  render () {
    return (
      <ul className="select-list">
        {this.renderAllItem()}
        {this.renderItems()}
      </ul>
    );
  },

  renderAllItem () {
    if (this.props.allItems) {
      var classes = classNames({
        selected: this.props.selected === null
      });

      var label = this.props.allLabel || 'All';

      return <li className={classes} onClick={this._listClick.bind(this, null)}>{label}</li>;
    }
  },

  renderItems () {
    if (this.props.items && this.props.items.length) {
      return this.props.items.map( (item, i) => {
        var classes = classNames({
          selected: this.props.selected === item.id
        });

        return <li className={classes} onClick={this._listClick.bind(this, item)} key={i}>{item.label}</li>;
      });
    }
  },

  _listClick (selected) {
    if (selected.id !== this.props.selected && typeof this.props.onSelect === 'function') {
      this.props.onSelect(selected);
    }
  }
});

export default SelectList;
