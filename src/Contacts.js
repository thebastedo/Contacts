import React from 'react'

import Input from 'react-bootstrap/lib/Input'
import Glyphicon from 'react-bootstrap/lib/Glyphicon'

import SelectList from './SelectList'


var Contacts = React.createClass({
  getInitialState () {
    return {
      search: null
    };
  },
  render () {
    let icon = <Glyphicon glyph="search" />;

    return (
      <div className="col-md-3 col-contacts">
        <Input
          ref="search"
          type="text"
          bsSize="small"
          value={this.state.search}
          addonBefore={icon}
          onChange={this._searchChanged} />
        {this.renderContacts()}
      </div>
    );
  },

  renderContacts () {
    var contacts = this._filterContacts();

    if (contacts && contacts.length) {
      contacts = contacts.map( (contact) => {
        return {id: contact.id, label: contact.first + ' ' + contact.last};
      });

      return <SelectList
        items={contacts}
        selected={this.props.selected}
        onSelect={this._contactSelected} />;
    }
  },

  _contactSelected (contact) {
    if (typeof this.props.onSelect === 'function') {
      this.props.onSelect(contact);
    }
  },

  _searchChanged () {
    this.setState({
      search: this.refs.search.getValue()
    });
  },

  _filterContacts () {
    var contacts = this.props.contacts;

    if (contacts && contacts.length && this.state.search) {
      contacts = this.props.contacts.filter( (contact) => {
        if (contact.first.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1) {
          return true;
        } else if (contact.last.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1) {
          return true;
        }

        return false;
      });
    }

    return contacts;
  }
});

export default Contacts;
