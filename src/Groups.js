import React from 'react'
import classNames from 'classnames'
import SelectList from './SelectList'

var Groups = React.createClass({
  getInitialState () {
    return {
      selected: null
    };
  },

  render () {
    return (
      <div className="col-md-2 col-groups">
        <SelectList
          items={this.props.groups}
          selected={this.props.selected}
          allItems={true}
          allLabel="All Contacts"
          onSelect={this._listClick} />
      </div>
    );
  },

  _listClick (group) {
    if (typeof this.props.onSelect === 'function') {
      console.log('Groups Click:', group);
      this.props.onSelect(group);
    }
  }
});

export default Groups;
