var React = require('react');

import Groups from './Groups'
import Contacts from './Contacts'
import Contact from './Contact'

import DemoContacts from './demo-contacts'

var ContactsApp = React.createClass({
  getInitialState () {
    return {
      selectedGroup: null,
      selectedContact: null
    };
  },

  render () {
    let groups = [ { id: 0, label: 'group 1' }, { id: 1, label: 'group 2' }, { id: 2, label: 'group 3' }, { id: 3, label: 'group 4' } ];

    let contacts = [ { id: 0, first: 'Bob', last: 'Smith' }, { id: 1, first: 'Jane', last: 'Johnson' } ];

    return (
      <div>
        <Groups groups={groups} selected={this.state.selectedGroup} onSelect={this._groupSelected} />
        <Contacts contacts={contacts} selected={this.state.selectedContact} onSelect={this._contactSelected} />
        <Contact />
      </div>
    );
  },

  _groupSelected (group) {
    this.setState({ selectedGroup: group.id });
  },

  _contactSelected (contact) {
    this.setState({ selectedContact: contact.id });
  }
});

export default ContactsApp;
