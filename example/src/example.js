var React = require('react');
var ReactDOM = require('react-dom');
var ContactsApp = require('react-contacts-app');

var App = React.createClass({
	render () {
		return (
			<div>
				<ContactsApp />
			</div>
		);
	}
});

ReactDOM.render(<App />, document.getElementById('app'));
